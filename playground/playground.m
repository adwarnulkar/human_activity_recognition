%% Loading the dataset

load ../dataset/train/X_train.txt;
load ../dataset/train/y_train.txt;

%% Plot the histogram of the output
% Purpose: To show that the samples are balanced
histogram(y_train);
grid on;
xlabel('Class'); ylabel('Frequency');
title('Histogram of class');