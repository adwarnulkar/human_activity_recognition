# Course        : EE660 Machine Learning
# University    : University of Southern California
# Author        : Aditya Warnulkar
# Email-ID      : warnulka@usc.edu
# Date          : 20th November 2017
# Description   : The intent of the code is to analyze the performance of Recursive Feature Elimination
#                 RFE is applied on Multinomial Logistic Regression.                  

# ## Importing Libraries

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats, integrate
from sklearn import linear_model, datasets
from sklearn.feature_selection import RFE
import time
import datetime
import os


# ## Output class dictionary

class_label = {
                1:'WALKING',
                2:'WALKING_UPSTAIRS',
                3:'WALKING_DOWNSTAIRS',
                4:'SITTING',
                5:'STANDING',
                6:'LAYING'
              };


# ## Log file creation 
ts = time.time();
directory = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d_%H_%M_%S');

log_path = './logs_RFE/';
dir_name = log_path + directory;
log_file_name = dir_name + '/' + 'session.log';

if not os.path.exists(dir_name):
    print 'Creating directory {}\n'.format(dir_name);
    os.makedirs(dir_name);

if os.path.exists('./logs_RFE/latest'):
    os.remove( './logs_RFE/latest');

os.symlink(directory ,log_path + '/latest');

log_file = open(log_file_name,'w');

# ## Loading Dataset (Training)

# Read the Feature Info
log_file.write('\nReading Data Set\n');
feature_info_file_name = "../dataset/features.txt";
feature_info_file = open(feature_info_file_name,'r');
feature_name = feature_info_file.read().splitlines();
print 'Number of Features : {}\n'.format(len(feature_name));

# Read the Training data
df_X_train = pd.read_csv('../dataset/train/X_train.txt',names=feature_name,header=None,delim_whitespace=True);
df_X_test  = pd.read_csv('../dataset/test/X_test.txt',names=feature_name,header=None,delim_whitespace=True);

# Read training outputs
df_y_train = pd.read_csv('../dataset/train/y_train.txt',names=['activity'],header=None);
df_y_test = pd.read_csv('../dataset/test/y_test.txt',names=['activity'],header=None);

log_file.write('\nCompleted Reading Data Set\n');


# ## Logistic Regression (Recursive Feature Elimination , L2 regularization)
# - Reference: http://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFE.html
# - Multinomial LR: https://en.wikipedia.org/wiki/Multinomial_logistic_regression
# - Feature extraction: https://machinelearningmastery.com/feature-selection-machine-learning-python/
# - Feature_elimination: http://scikit-learn.org/stable/modules/feature_selection.html
# 
# value of lambda = 1


# Extract training features and outputs
X_train = df_X_train.values;
y_train = df_y_train.activity;
X_test = df_X_test.values;
y_test = df_y_test.activity;


# #### Fit Logistic regression model


# Fit Multinomial regression model
log_file.write('\nPerforming RFE (single run)\n');
logreg = linear_model.LogisticRegression(); #Lambda = 1
logreg_RFE = RFE(logreg,n_features_to_select=100,step=50);
RFE_model = logreg_RFE.fit(X_train,y_train);

log_file.write('Number of Features : {}\n'.format(RFE_model.n_features_));
log_file.write('\nCompleted RFE (single run)\n');


# #### Predict the outputs and measure performance
# - Performance = Percentage of accurately predicted samples


# Predict outcomes for training and testing data
y_hat_train = logreg_RFE.predict(X_train);
y_hat_test = logreg_RFE.predict(X_test);

# Measure model accuracy on training and testing data
accuracy_train = 100*np.mean(y_hat_train==y_train);
accuracy_test  = 100*np.mean(y_hat_test==y_test);

log_file.write('Accuracy on Training data is {} %\n'.format(accuracy_train));
log_file.write('Accuracy on Testing data  is {} %\n'.format(accuracy_test));

plt.figure(1);
plt.hist(RFE_model.ranking_)
plt.xlabel('Feature Rank');
plt.ylabel('Frequency');
plt.title('Feature Rank histogram');
plt.savefig(dir_name + '/' + 'feature_rank_histogram');


# ## Logistic Regression L2 regularization (Feature Selection Sweep)
# Value of lambda = 1


# Fit Multinomial regression model
print '\nL2 Regularizer feature sweep : START\n';
log_file.write('\nL2 REGULARIZER FEATURE SWEEP\n');
num_features = np.arange(50,550,50);
accuracy_train_list_l2 = list();
accuracy_test_list_l2 = list();

for n in num_features:
    
    print 'Fitting model for {} Features\n'.format(n);
    logreg = linear_model.LogisticRegression(); #Lambda = 1
    logreg_RFE = RFE(logreg,n_features_to_select=n,step=50);
    logreg_RFE.fit(X_train,y_train);
    
    y_hat_train = logreg_RFE.predict(X_train);
    y_hat_test = logreg_RFE.predict(X_test);
    
    # Measure model accuracy on training and testing data
    accuracy_train = 100*np.mean(y_hat_train==y_train);
    accuracy_test  = 100*np.mean(y_hat_test==y_test);
    accuracy_train_list_l2.append(accuracy_train);
    accuracy_test_list_l2.append(accuracy_test);
    log_file.write('\nTraining accuracy for {} feature : {} %\n'.format(n,accuracy_train));
    log_file.write('Testing  accuracy for {} feature : {} %\n'.format(n,accuracy_test));


print '\nL2 Regularizer feature sweep : END\n';

plt.figure(2);
plt.plot(num_features,accuracy_train_list_l2,'o--');
plt.plot(num_features,accuracy_test_list_l2,'o--');
plt.xlabel('Number of features');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Prediction Accuracy v/s number of feature (L2)');
plt.savefig(dir_name + '/' + 'feature_sweep_L2');


# ## Logistic Regression L1 regularization (Feature Selection Sweep)
# Value of lambda = 1


# Fit Multinomial regression model
print '\nL1 Regularizer feature sweep : START\n';
log_file.write('\nL1 REGULARIZER FEATURE SWEEP\n');
num_features = np.arange(50,550,50);
accuracy_train_list_l1 = list();
accuracy_test_list_l1 = list();

for n in num_features:
    
    print 'Fitting model for {} Features\n'.format(n);
    logreg = linear_model.LogisticRegression(penalty='l1'); #Lambda = 1
    logreg_RFE = RFE(logreg,n_features_to_select=n,step=50);
    logreg_RFE.fit(X_train,y_train);
    
    y_hat_train = logreg_RFE.predict(X_train);
    y_hat_test = logreg_RFE.predict(X_test);
    
    # Measure model accuracy on training and testing data
    accuracy_train = 100*np.mean(y_hat_train==y_train);
    accuracy_test  = 100*np.mean(y_hat_test==y_test);
    accuracy_train_list_l1.append(accuracy_train);
    accuracy_test_list_l1.append(accuracy_test);
    log_file.write('\nTraining accuracy for {} feature : {} %\n'.format(n,accuracy_train));
    log_file.write('Testing  accuracy for {} feature : {} %\n'.format(n,accuracy_test));


print '\nL1 Regularizer feature sweep : END\n';

plt.figure(3);
plt.plot(num_features,accuracy_train_list_l1,'o--');
plt.plot(num_features,accuracy_test_list_l1,'o--');
plt.xlabel('Number of features');
plt.ylabel('Prediction Accuracy (%)');
plt.legend(['Train','Test']);
plt.title('Prediction Accuracy v/s number of feature (L1)');
plt.savefig(dir_name + '/' + 'feature_sweep_L1');
#plt.show();


# ## Comparing L1 and L2 RFE models

print '\nPlotting L1 L2 comparision plots\n';
plt.figure(4);
plt.plot(num_features,accuracy_train_list_l1,'o--');
plt.plot(num_features,accuracy_train_list_l2,'o--');
plt.xlabel('Number of features');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Training prediction accuracy comparision (L1 v/s L2)');
plt.legend(['Train L1','Train L2']);
plt.savefig(dir_name + '/' + 'L1_L2_RFE_comparision_train');
#plt.show();

plt.figure(5);
plt.plot(num_features,accuracy_test_list_l1,'o--');
plt.plot(num_features,accuracy_test_list_l2,'o--');
plt.xlabel('Number of features');
plt.ylabel('Prediction Accuracy (%)');
plt.legend(['Test L1','Test L2']);
plt.title('Testing prediction accuracy comparision (L1 v/s L2)');
plt.savefig(dir_name + '/' + 'L1_L2_RFE_comparision_test');
#plt.show();


# ## Plotting correlation grid of top 4 features


# Fit Multinomial regression model
print '\nPlotting Grid plot for Top 4 features\n';
logreg = linear_model.LogisticRegression(); #Lambda = 1
logreg_RFE = RFE(logreg,n_features_to_select=4,step=10);
RFE_model = logreg_RFE.fit(X_train,y_train);

a = np.arange(0,561);
top = a[RFE_model.ranking_ == 1];
feature_name_top = [feature_name[x] for x in top];

df_temp = df_X_train[feature_name_top];
df_temp['activity_label'] = df_y_train['activity'];
df_temp['activity_label'].replace(class_label, inplace=True);

sns_plot = sns.pairplot(df_temp,hue='activity_label');
sns_plot.savefig(dir_name + '/' + 'top_features_grid_plot')

log_file.close();

#plt.show();

print '\nCOMPLETED LOGISTIC REGRESSION WITH RFE\n';

# # Conclusion
# - From the above plot it can be seen that prediction accuracy doesn't drop over 1% for
#   number of features as low as 100
