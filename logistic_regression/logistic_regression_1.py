# Course        : EE660 Machine Learning
# University    : University of Southern California
# Author        : Aditya Warnulkar
# Email-ID      : warnulka@usc.edu
# Date          : 20th November 2017
# Description   : The intent of the code is to analyze the performance of Multinomial Logistic Regression                  


# ## Importing Libraries

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats, integrate
from sklearn import linear_model, datasets
import time
import datetime
import os


# ## Output class dictionary


class_label = {
                1:'WALKING',
                2:'WALKING_UPSTAIRS',
                3:'WALKING_DOWNSTAIRS',
                4:'SITTING',
                5:'STANDING',
                6:'LAYING'
              };


# ## Log file creation 
ts = time.time();
directory = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d_%H_%M_%S');

log_path = './logs_LG/';
dir_name = log_path + directory;
log_file_name = dir_name + '/' + 'session.log';

if not os.path.exists(dir_name):
    print 'Creating directory {}\n'.format(dir_name);
    os.makedirs(dir_name);

if os.path.exists('./logs_LG/latest'):
    os.remove( './logs_LG/latest');

os.symlink(directory ,log_path + '/latest');

log_file = open(log_file_name,'w');



# ## Loading Dataset (Training)


# Read the Feature Info
print '\nLoading data set\n';
feature_info_file_name = "../dataset/features.txt";
feature_info_file = open(feature_info_file_name,'r');
feature_name = feature_info_file.read().splitlines();
print 'Number of Features : {}\n'.format(len(feature_name));

# Read the Training data
df_X_train = pd.read_csv('../dataset/train/X_train.txt',names=feature_name,header=None,delim_whitespace=True);
df_X_test  = pd.read_csv('../dataset/test/X_test.txt',names=feature_name,header=None,delim_whitespace=True);

# Read training outputs
df_y_train = pd.read_csv('../dataset/train/y_train.txt',names=['activity'],header=None);
df_y_test = pd.read_csv('../dataset/test/y_test.txt',names=['activity'],header=None);
print '\nLoading data set complete\n';


# ## Logistic Regression (all features , L2 regularization)
# - Reference: http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html
# - Multinomial LR: https://en.wikipedia.org/wiki/Multinomial_logistic_regression
# 
# value of lambda = 1


print '\nStarting Logistic regression (L2 regularization)\n';
log_file.write('Logistic Regression (L2 Regularization)\n')
# Extract training features and outputs
X_train = df_X_train.values;
y_train = df_y_train.activity;
X_test = df_X_test.values;
y_test = df_y_test.activity;


# #### Fit Logistic regression model


# Fit Multinomial regression model
logreg = linear_model.LogisticRegression(); #Lambda = 1
logreg.fit(X_train,y_train);


# #### Predict the outputs and measure performance
# - Performance = Percentage of accurately predicted samples


# Predict outcomes for training and testing data
y_hat_train = logreg.predict(X_train);
y_hat_test = logreg.predict(X_test);

# Measure model accuracy on training and testing data
accuracy_train = 100*np.mean(y_hat_train==y_train);
accuracy_test  = 100*np.mean(y_hat_test==y_test);

log_file.write('\nAccuracy on Training data is {} % \n'.format(accuracy_train));
log_file.write('Accuracy on Testing data  is {} % \n'.format(accuracy_test));

print '\nCompleted Logistic regression (L2 regularization)\n';


# ## Logistic Regression (all features , L1 regularization)
# Value of lambda = 1

print '\nStarted Logistic regression (L1 regularization)\n';
log_file.write('Logistic Regression (L1 Regularization)\n')

# Fit Multinomial regression model
logreg_l1 = linear_model.LogisticRegression(penalty='l1'); #Lambda = 1
logreg_l1.fit(X_train,y_train);

# Predict outcomes for training and testing data
y_hat_train = logreg_l1.predict(X_train);
y_hat_test = logreg_l1.predict(X_test);

# Measure model accuracy on training and testing data
accuracy_train = 100*np.mean(y_hat_train==y_train);
accuracy_test  = 100*np.mean(y_hat_test==y_test);

log_file.write('\nAccuracy on Training data is {} %\n'.format(accuracy_train));
log_file.write('Accuracy on Testing data  is {} %\n'.format(accuracy_test));

print '\nCompleted Logistic regression (L1 regularization)\n';


# ## Logistic regression with L2 regularizer (lambda sweep)
# The goal of this section is to perform logistic regression with various values of lambda.  
# The regularizer used is **L2**

print '\nStarted Logistic regression , L2 regularization , lambda sweep\n';
log_file.write('\nLogistic Regression (L2 Regularization , lambda sweep)\n')

lmda = np.arange(-5,3,dtype=float);
lmda = np.power(10,lmda);
reg_param = np.power(lmda,-1);
accuracy_train_list_l2 = list();
accuracy_test_list_l2 = list();

for c in reg_param:
    # Model fitting
    logreg = linear_model.LogisticRegression(C=c);
    logreg.fit(X_train,y_train);
    
    # Output prediction
    y_hat_train = logreg.predict(X_train);
    y_hat_test = logreg.predict(X_test);
    
    # Performance measurement
    accuracy_train = 100*np.mean(y_hat_train==y_train);
    accuracy_test  = 100*np.mean(y_hat_test==y_test);
    
    accuracy_train_list_l2.append(accuracy_train);
    accuracy_test_list_l2.append(accuracy_test);

    log_file.write('\nLambda value {} ; Training accuracy : {}\n'.format(1.0/c,accuracy_train));
    log_file.write('Lambda value {} ; Training accuracy : {}\n'.format(1.0/c,accuracy_test));


print '\nCompleted Logistic regression , L2 regularization , lambda sweep\n';

# Plot performance for various regularization parameters

print '\nPlotting sweep results for L2 regularizer : START\n';

plt.figure(1);
plt.plot(np.log10(lmda),accuracy_train_list_l2,'o--');
plt.plot(np.log10(lmda),accuracy_test_list_l2,'o--');
plt.xlabel('log lambda');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Prediction accuracy for L2 regularizer');
plt.legend(['Training','Testing']);
plt.savefig(dir_name + '/' + 'L2_regularizer');
#plt.show();

print '\nPlotting sweep results for L2 regularizer : END\n';


# ## Logistic regression with L1 regularizer (lambda sweep)
# The goal of this section is to perform logistic regression with various values of lambda.  
# The regularizer used is **L1**

print '\nStarted Logistic regression , L1 regularization , lambda sweep\n';
log_file.write('\nLogistic Regression (L1 Regularization , lambda sweep)\n')

lmda = np.arange(-5,3,dtype=float);
lmda = np.power(10,lmda);
reg_param = np.power(lmda,-1);
accuracy_train_list_l1 = list();
accuracy_test_list_l1 = list();

for c in reg_param:
    # Model fitting
    logreg = linear_model.LogisticRegression(penalty='l1',C=c);
    logreg.fit(X_train,y_train);
    
    # Output prediction
    y_hat_train = logreg.predict(X_train);
    y_hat_test = logreg.predict(X_test);
    
    # Performance measurement
    accuracy_train = 100*np.mean(y_hat_train==y_train);
    accuracy_test  = 100*np.mean(y_hat_test==y_test);
    
    accuracy_train_list_l1.append(accuracy_train);
    accuracy_test_list_l1.append(accuracy_test);

    log_file.write('\nLambda value {} ; Training accuracy : {}\n'.format(1.0/c,accuracy_train));
    log_file.write('Lambda value {} ; Training accuracy : {}\n'.format(1.0/c,accuracy_test));


print '\nCompleted Logistic regression , L1 regularization , lambda sweep\n';


# Plot performance for various regularization parameters

print '\nPlotting sweep results for L1 regularizer : START\n';

plt.figure(2);
plt.plot(np.log10(lmda),accuracy_train_list_l1,'o--');
plt.plot(np.log10(lmda),accuracy_test_list_l1,'o--');
plt.xlabel('log lambda');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Prediction accuracy for L1 regularizer');
plt.legend(['Training','Testing']);
plt.savefig(dir_name + '/' + 'L1_regularizer');
#plt.show();

print '\nPlotting sweep results for L1 regularizer : END\n';


# ## Comparison of L1 v/s L2 regularizer

print '\nComparing L1 and L2 regularizer performance : START\n';

# Plot combined performance (L1 versus L2)
plt.figure(3);
plt.plot(np.log10(lmda),accuracy_test_list_l1,'o--');
plt.plot(np.log10(lmda),accuracy_test_list_l2,'o--');
plt.xlabel('log lambda');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Testing accuracy (L1 v/s L2)');
plt.legend(['L1 regularizer','L2 Regularizer']);
plt.savefig(dir_name + '/' + 'L1_L2_test_comparison');
#plt.show();

print '\nComparing L1 and L2 regularizer performance : END\n';

print '\nLogistic regression performed successfully\n';


# # Conclusion
# - From the above plot it can be seen that best testing accuracy is obtained for L1 regularizer with lambda = 0.1
# - As lambda increases training accuracy decreases , but testing accuracy first increases, reaches a maximum value and then decreases. This maximum value is what we are looking for. The motivation to perform lambda sweep was to find the optimum value of lambda which gives us the best accuracy on the testing data
