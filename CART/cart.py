# Course        : EE660 Machine Learning
# University    : University of Southern California
# Author        : Aditya Warnulkar
# Email-ID      : warnulka@usc.edu
# Date          : 2nd December 2017
# Description   : The intent of the code is to analyze the performance of CART                 

# ## Importing Libraries

# In[9]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats, integrate
from sklearn import linear_model, datasets
from sklearn.feature_selection import RFE
from sklearn import tree
import graphviz
#get_ipython().magic(u'matplotlib inline')


# ## Output class dictionary

# In[10]:

class_label = {
                1:'WALKING',
                2:'WALKING_UPSTAIRS',
                3:'WALKING_DOWNSTAIRS',
                4:'SITTING',
                5:'STANDING',
                6:'LAYING'
              };


# ## Loading Dataset (Training)

# In[11]:

# Read the Feature Info
feature_info_file_name = "../dataset/features.txt";
feature_info_file = open(feature_info_file_name,'r');
feature_name = feature_info_file.read().splitlines();
print 'Number of Features : {}\n'.format(len(feature_name));

# Read the Training data
df_X_train = pd.read_csv('../dataset/train/X_train.txt',names=feature_name,header=None,delim_whitespace=True);
df_X_test  = pd.read_csv('../dataset/test/X_test.txt',names=feature_name,header=None,delim_whitespace=True);

# Read training outputs
df_y_train = pd.read_csv('../dataset/train/y_train.txt',names=['activity'],header=None);
df_y_test = pd.read_csv('../dataset/test/y_test.txt',names=['activity'],header=None);


# In[12]:

df_X_train.head()


# ## Classification and Regression Tree
# - API Reference: http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier
# - Example: http://scikit-learn.org/stable/modules/tree.html

# In[13]:

# Extract training features and outputs
X_train = df_X_train.values;
y_train = df_y_train.activity;
X_test = df_X_test.values;
y_test = df_y_test.activity;


# In[14]:

# Fit CART model
clf = tree.DecisionTreeClassifier(min_samples_leaf=10,min_samples_split=10);
clf = clf.fit(X_train, y_train);


# In[15]:

# Predict the output
y_hat_train = clf.predict(X_train);
y_hat_test  = clf.predict(X_test);


# In[16]:

# Measure model performance
accuracy_train = 100*np.mean(y_hat_train==y_train);
accuracy_test = 100*np.mean(y_hat_test==y_test);
print ('Training Accuracy is {} %'.format(accuracy_train));
print ('Testing Accuracy is {} %'.format(accuracy_test));


# ## Decision Tree
#dot_data = tree.export_graphviz(clf, out_file=None);
#graph = graphviz.Source(dot_data);
#graph.render("cart")

# # Conclusion
# - From the performance results, it can be seen that CART provides very bad out of sample accuracy. 
# - This is the reason we use Random Forest
