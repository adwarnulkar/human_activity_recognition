% University of Southern California
% email address: warnulka@usc.edu
% November 2017; Last revision: 21-Nov-2017

% Coursename : EE-660 (Machine Learning)

clc ; clear ; close all;

%% Load the pmtk3 package and data set

addpath(genpath('/Users/adwarnulkar/Development/MATLAB/EE-660 (Machine Learning)/pmtk3'));
%run initPmtk3.m;

% Load the data set
load ../dataset/train/X_train.txt;
load ../dataset/train/y_train.txt;
load ../dataset/test/X_test.txt;
load ../dataset/test/y_test.txt;

D_train = [X_train y_train];
D_test = [X_test y_test];

fprintf('Number of Training samples : %d\n',size(D_train,1));
fprintf('Number of Testing samples : %d\n',size(D_test,1));
fprintf('Number of Features : %d\n',size(X_train,2));

%% Initialize training parameters

Ntrain = 7000;
random_features = 50;
bag_size = 1/3;
ntrees = 30;

% Choose Ntrain random samples from dataset
%D = D_train(randsample(length(y_train),Ntrain),:);
%x_train = D(:,1:end-1);
%y_train = D(:,end);

% Apply Random Forest
forest = fitForest(X_train,y_train,'randomFeatures',random_features,'bagSize',bag_size,'ntrees',30);

% Predict output for training data set
y_train_hat = predictForest(forest,X_train);

% Predict output for testing data set
y_test_hat = predictForest(forest,X_test);

% Evaluate errors
accuracy_train = 100*mean(y_train_hat == y_train);
accuracy_test = 100*mean(y_test_hat == y_test);
fprintf('Training error is %0.2f\n',accuracy_train);
fprintf('Testing error is %0.2f\n',accuracy_test);


%% Plot the results

load results.mat;

figure('Name','Mean Error Rate (Training)');
plot(mean_error_train,'r*-','MarkerSize',10,'LineWidth',2.0);
grid on;
xlabel('ntree');
ylabel('Mean Error Rate % (Training)');

figure('Name','Mean Error Rate (Testing)');
plot(mean_error_test,'r*-','MarkerSize',10,'LineWidth',2.0);
grid on;
xlabel('ntree');
ylabel('Mean Error Rate % (Test)');

figure('Name','Standard deviation of error rate (Test)');
plot(std_error_test,'r*-','MarkerSize',10,'LineWidth',2.0);
grid on;
xlabel('ntree');
ylabel('Standard Deviation of error rate (Test)');

%% Justification for lower error rate as ntree increases

clc ; clear ; close all;

N_range = 2:2:30;
p = 0.7;
y = zeros(1,length(N_range));

count = 1;
for N = N_range
    
    for k = (N/2):N
        y(count) = y(count) + nchoosek(N,k)*(p^k)*((1-p)^(N-k));
    end
    
    count = count + 1;
    
end

figure('Name','Probability of misclassification');
plot(1-y,'r*-','MarkerSize',15,'LineWidth',2.0);
xlabel('Number of Trees');
ylabel('prob of misclassification');
grid on;

figure('Name','Probability of classification');
plot(y,'b*-','MarkerSize',15,'LineWidth',2.0);
xlabel('Number of Trees');
ylabel('prob of classification');
grid on;

figure('Name','std of error rate');
plot(sqrt(10*y.*(1-y)),'g*-','MarkerSize',15,'LineWidth',2.0);
xlabel('Number of Trees');
ylabel('standard deviation of misclassification');
grid on;
