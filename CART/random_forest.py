# Course        : EE660 Machine Learning
# University    : University of Southern California
# Author        : Aditya Warnulkar
# Email-ID      : warnulka@usc.edu
# Date          : 3rd December 2017
# Description   : The intent of the code is to analyze the performance of Random Forest Technique                  

# ## Importing Libraries

# In[2]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats, integrate
from sklearn import linear_model, datasets
from sklearn.feature_selection import RFE
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
import graphviz
import time
import datetime
import os
#get_ipython().magic(u'matplotlib inline')


# ## Output class dictionary

# In[3]:

class_label = {
                1:'WALKING',
                2:'WALKING_UPSTAIRS',
                3:'WALKING_DOWNSTAIRS',
                4:'SITTING',
                5:'STANDING',
                6:'LAYING'
              };


# ## Log file creation 
ts = time.time();
directory = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d_%H_%M_%S');

log_path = './logs_RF/';
dir_name = log_path + directory;
log_file_name = dir_name + '/' + 'session.log';

if not os.path.exists(dir_name):
    print 'Creating directory {}\n'.format(dir_name);
    os.makedirs(dir_name);

if os.path.exists('./logs_RF/latest'):
    os.remove( './logs_RF/latest');

os.symlink(directory ,log_path + '/latest');

log_file = open(log_file_name,'w');


# ## Loading Dataset (Training)

# In[4]:

# Read the Feature Info
feature_info_file_name = "../dataset/features.txt";
feature_info_file = open(feature_info_file_name,'r');
feature_name = feature_info_file.read().splitlines();
print 'Number of Features : {}\n'.format(len(feature_name));

# Read the Training data
df_X_train = pd.read_csv('../dataset/train/X_train.txt',names=feature_name,header=None,delim_whitespace=True);
df_X_test  = pd.read_csv('../dataset/test/X_test.txt',names=feature_name,header=None,delim_whitespace=True);

# Read training outputs
df_y_train = pd.read_csv('../dataset/train/y_train.txt',names=['activity'],header=None);
df_y_test = pd.read_csv('../dataset/test/y_test.txt',names=['activity'],header=None);


# In[5]:

#df_X_train.head()


# ## Random Forest
# - API Reference: http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html

# In[6]:

# Extract training features and outputs
X_train = df_X_train.values;
y_train = df_y_train.activity;
X_test = df_X_test.values;
y_test = df_y_test.activity;


# In[7]:

# Fit Random Forest model
#clf = RandomForestClassifier(n_estimators=500,min_samples_split=20,min_samples_leaf=20);
clf = RandomForestClassifier(n_estimators=100);
clf = clf.fit(X_train, y_train);


# In[8]:

# Predict the output
y_hat_train = clf.predict(X_train);
y_hat_test  = clf.predict(X_test);


# In[9]:

# Measure model performance
accuracy_train = 100*np.mean(y_hat_train==y_train);
accuracy_test = 100*np.mean(y_hat_test==y_test);
log_file.write ('Training Accuracy is {} %\n'.format(accuracy_train));
log_file.write ('Testing Accuracy is {} %\n'.format(accuracy_test));


# ## Random Forest (ntree sweep)

# In[10]:

accuracy_train_list = list();
accuracy_test_list = list();
ntree_array = np.arange(10,210,10);

for n in ntree_array:
    
    print('Performing Random Forest for n_tree : {}'.format(n));
    
    # Fit Random Forest Model
    clf = RandomForestClassifier(n_estimators=n);
    clf = clf.fit(X_train,y_train);
    
    # Predict the output
    y_hat_train = clf.predict(X_train);
    y_hat_test = clf.predict(X_test);
    
    # Measure the performance
    accuracy_train = 100*np.mean(y_hat_train==y_train);
    accuracy_test = 100*np.mean(y_hat_test==y_test);
    accuracy_train_list.append(accuracy_train);
    accuracy_test_list.append(accuracy_test);


# ## Performance analysis

# In[16]:
log_file.write('Plotting results\n');
plt.figure(1);
plt.plot(ntree_array,accuracy_train_list,'o--');
plt.xlabel('Number of Trees');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Training Prediction Accuracy v/s number of trees');
plt.savefig(dir_name + '/' + 'Training_Accuracy');

plt.figure(2);
plt.plot(ntree_array,accuracy_test_list,'go--');
plt.xlabel('Number of Trees');
plt.ylabel('Prediction Accuracy (%)');
plt.title('Testing Prediction Accuracy v/s number of trees');
plt.savefig(dir_name + '/' + 'Testing_Accuracy');

#plt.show();



# # Conclusion
# - Random Forest reduces variancein the error and hence is a better predictor than single tree CART
