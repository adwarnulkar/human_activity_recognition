# Steps to clone the project
- Install GIT on your system
- git clone https://adwarnulkar@bitbucket.org/adwarnulkar/human_activity_recognition.git

# Required tools to run this project
- Python 2.7 or above
- Anaconda 2.0 or higher: (https://www.anaconda.com/download/#macos)
- Numpy for array manipulation (conda install -c anaconda numpy)
- Pandas for data analysis (conda install -c anaconda pandas)
- Matplotlib for plotting (conda install -c conda-forge matplotlib)
- Seaborn for plotting (conda install -c anaconda seaborn)
- scikit learn for machine learning package (conda install -c anaconda scikit-learn)
- Jupyter notebook (conda install -c anaconda jupyter)

# Logistic Regression (full feature set)
- cd logistic_regression/
- python logistic_regression_1.py
- Log path and plots : cd ./logs_LG/latest

# Logistic Regression (Recursive Feature Elimination)
- cd logistic_regression/
- python logistic_regression_RFE.py
- Log path and plots : cd ./logs_RFE/latest

# CART (single decision tree)
- cd CART/
- python cart.py
- No plots for this model

# Random Forest
- cd CART/
- python random_forest.py
- Log path and plots : cd ./logs_RF/latest

# Support Vector Machine
- cd SVM
- python support_vector_machine.py
- Log path and plots : cd logs_SVM/latest

# Data set
- Training : cd dataset/train/
- Testing  : cd dataset/test/
- features : dataset/features.txt
- feature description : dataset/features_info.txt

# Project owner
- Name : Aditya Warnulkar
- Email : warnulka@usc.edu


