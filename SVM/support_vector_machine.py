# Course        : EE660 Machine Learning
# University    : University of Southern California
# Author        : Aditya Warnulkar
# Email-ID      : warnulka@usc.edu
# Date          : 3rd December 2017
# Description   : The intent of the code is to analyze the performance of SVM                  


# ## Importing Libraries

# In[9]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats, integrate
from sklearn import linear_model, datasets
from sklearn.feature_selection import RFE
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
import time
import datetime
import os
#get_ipython().magic(u'matplotlib inline')


# ## Output class dictionary

# In[10]:

class_label = {
                1:'WALKING',
                2:'WALKING_UPSTAIRS',
                3:'WALKING_DOWNSTAIRS',
                4:'SITTING',
                5:'STANDING',
                6:'LAYING'
              };

# ## Log file creation 
ts = time.time();
directory = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d_%H_%M_%S');

log_path = './logs_SVM/';
dir_name = log_path + directory;
log_file_name = dir_name + '/' + 'session.log';

if not os.path.exists(dir_name):
    print 'Creating directory {}\n'.format(dir_name);
    os.makedirs(dir_name);

if os.path.exists('./logs_SVM/latest'):
    os.remove( './logs_SVM/latest');

os.symlink(directory ,log_path + '/latest');

log_file = open(log_file_name,'w');



# ## Loading Dataset (Training)

# In[11]:

# Read the Feature Info
feature_info_file_name = "../dataset/features.txt";
feature_info_file = open(feature_info_file_name,'r');
feature_name = feature_info_file.read().splitlines();
print 'Number of Features : {}\n'.format(len(feature_name));

# Read the Training data
df_X_train = pd.read_csv('../dataset/train/X_train.txt',names=feature_name,header=None,delim_whitespace=True);
df_X_test  = pd.read_csv('../dataset/test/X_test.txt',names=feature_name,header=None,delim_whitespace=True);

# Read training outputs
df_y_train = pd.read_csv('../dataset/train/y_train.txt',names=['activity'],header=None);
df_y_test = pd.read_csv('../dataset/test/y_test.txt',names=['activity'],header=None);


# In[12]:

#df_X_train.head()


# ## Support Vector Machine
# - API Reference: http://scikit-learn.org/stable/modules/svm.html
# - Example: https://pythonspot.com/en/support-vector-machine/

# In[13]:

# Extract training features and outputs
X_train = df_X_train.values;
y_train = df_y_train.activity;
X_test = df_X_test.values;
y_test = df_y_test.activity;


# In[14]:

# Fit SVM model
log_file.write('Performing SVM with Linear Kernel\n');
C = 1;
svc = svm.SVC(kernel='linear', C=C);
#svc = svm.SVC(kernel='rbf', gamma=0.7, C=C);
svc = svc.fit(X_train, y_train);


# In[15]:

# Predict the output
y_hat_train = svc.predict(X_train);
y_hat_test  = svc.predict(X_test);


# In[16]:

# Measure model performance
accuracy_train = 100*np.mean(y_hat_train==y_train);
accuracy_test = 100*np.mean(y_hat_test==y_test);
print ('Training Accuracy is {} %'.format(accuracy_train));
print ('Testing Accuracy is {} %'.format(accuracy_test));


# ## Support Vector Machine (Polynomial Kernel)

# In[17]:

# Fit SVM model
log_file.write('Performing SVM degree sweep\n');
C = 1;
max_degree = 6;
accuracy_train_list = list();
accuracy_test_list = list();

for d in range(2,max_degree+1):
    
    print('Fitting SVM model with poly degree {}'.format(d));
    
    # Fit the model
    svc = svm.SVC(kernel='poly', degree=d, C=C);
    svc = svc.fit(X_train, y_train);
    
    # Predict the output
    y_hat_train = svc.predict(X_train);
    y_hat_test  = svc.predict(X_test);
    
    # Measure model performance
    accuracy_train = 100*np.mean(y_hat_train==y_train);
    accuracy_test = 100*np.mean(y_hat_test==y_test);
    accuracy_train_list.append(accuracy_train);
    accuracy_test_list.append(accuracy_test);


# In[21]:

log_file.write('Plotting Degree sweep\n');
# Plot results
plt.figure(1);
#plt.plot(range(2,max_degree+1),accuracy_train_list,'o--');
plt.plot(range(2,max_degree+1),accuracy_test_list,'go--');
plt.xlabel('tBody.AccJerkMag_iqr');
plt.ylabel('tBodyAccJerk_entropy_X');
plt.title('Prediction Accuracy v/s SVM kernel Poly degree');
plt.savefig(dir_name + '/' + 'svm_degree_sweep');
#plt.legend(['Training','Testing']);
#plt.show();


# ## SVM visualization for 2 feature case

# In[33]:
log_file.write('Visualizing SVM\n');
X_small = X_train[:,[233,102]];
plt.figure(2)
plt.scatter(X_small[:,0],X_small[:,1]);
plt.xlabel('Polynomial Degree');
plt.ylabel('Prediction Accuracy (%)');
plt.savefig(dir_name + '/' + 'top_feature_scatter');


# In[34]:

def make_meshgrid(x, y, h=.02):
    """Create a mesh of points to plot in

    Parameters
    ----------
    x: data to base x-axis meshgrid on
    y: data to base y-axis meshgrid on
    h: stepsize for meshgrid, optional

    Returns
    -------
    xx, yy : ndarray
    """
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    return xx, yy

def plot_contours(ax, clf, xx, yy, **params):
    """Plot the decision boundaries for a classifier.

    Parameters
    ----------
    ax: matplotlib axes object
    clf: a classifier
    xx: meshgrid ndarray
    yy: meshgrid ndarray
    params: dictionary of params to pass to contourf, optional
    """
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx, yy, Z, **params)
    return out


# In[38]:

models = (svm.SVC(kernel='linear', C=C),
          svm.LinearSVC(C=C),
          svm.SVC(kernel='rbf', gamma=0.7, C=C),
          svm.SVC(kernel='poly', degree=2, C=C))
models = (clf.fit(X_small, y_train) for clf in models)

# title for the plots
titles = ('SVC with linear kernel',
          'LinearSVC (linear kernel)',
          'SVC with RBF kernel',
          'SVC with polynomial (degree 2) kernel')

# Set-up 2x2 grid for plotting.
plt.figure(3);
fig, sub = plt.subplots(2, 2)
plt.subplots_adjust(wspace=0.4, hspace=0.4)

X0, X1 = X_small[:, 0], X_small[:, 1]
xx, yy = make_meshgrid(X0, X1)

for clf, title, ax in zip(models, titles, sub.flatten()):
    plot_contours(ax, clf, xx, yy,
                  cmap=plt.cm.coolwarm, alpha=0.8)
    ax.scatter(X0, X1, c=y_train, cmap=plt.cm.coolwarm, s=10, edgecolors='k')
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xlabel('tBody.AccJerkMag_iqr')
    ax.set_ylabel('tBodyAccJerk_entropy_X')
    ax.set_xticks(())
    ax.set_yticks(())
    ax.set_title(title)

plt.savefig(dir_name + '/' + 'svm_visualization');
#plt.show()

